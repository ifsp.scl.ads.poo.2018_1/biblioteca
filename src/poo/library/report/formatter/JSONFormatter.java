package poo.library.report.formatter;

import poo.library.common.model.Mapable;

import java.util.List;
import java.util.StringJoiner;

public class JSONFormatter implements Formatter {
    @Override
    public String formatMapables(List<? extends Mapable> mapables) {
        StringJoiner objectJoiner = new StringJoiner(",", "[", "]");

        mapables.forEach(
                mapable -> {
                    StringJoiner propertyJoiner = new StringJoiner(",", "{", "}");
                    mapable.mappedProperties().forEach(
                            (key, value) -> propertyJoiner.add(
                                    "\"" + key + "\":\"" + value + "\""
                            )
                    );
                    objectJoiner.add(propertyJoiner.toString());
                }
        );
        return objectJoiner.toString();
    }

    @Override
    public String formatFileExtension() {
        return "json";
    }
}
