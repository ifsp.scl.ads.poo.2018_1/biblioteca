package poo.library.report.formatter;

import poo.library.common.model.Mapable;

import java.util.List;

public interface Formatter {
    public String formatMapables(List<? extends Mapable> mapables);
    public String formatFileExtension();
}
