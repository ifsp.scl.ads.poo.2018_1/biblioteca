package poo.library.report.formatter;

import poo.library.common.model.Mapable;

import java.util.*;

public class CSVFormatter implements Formatter {
    @Override
    public String formatMapables(List<? extends Mapable> mapables) {
        SortedSet<String> keys = new TreeSet<>();
        mapables.forEach(
                mapable -> keys.addAll(
                        mapable.mappedProperties().keySet()
                )
        );
        StringJoiner headerJoiner = new StringJoiner("\",\"", "\"", "\"" + System.getProperty("line.separator"));
        keys.forEach(headerJoiner::add);

        StringBuilder builder = new StringBuilder(headerJoiner.toString());

        mapables.forEach(
                mapable -> {
                    keys.forEach(
                            key -> {
                                builder.append("\"");
                                if (mapable.mappedProperties().containsKey(key)) {
                                    builder.append(mapable.mappedProperties().get(key));
                                }
                                builder.append("\",");
                            }
                    );
                    builder.setLength(builder.length() - 1);
                    builder.append(System.getProperty("line.separator"));
                }
        );

        return builder.toString().trim();
    }

    @Override
    public String formatFileExtension() {
        return "csv";
    }
}
