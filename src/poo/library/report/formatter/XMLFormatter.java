package poo.library.report.formatter;

import poo.library.common.model.Mapable;

import java.util.List;
import java.util.StringJoiner;

public class XMLFormatter implements Formatter {
    @Override
    public String formatMapables(List<? extends Mapable> mapables) {
        StringJoiner objectJoiner = new StringJoiner("", "<root>", "</root>");

        mapables.forEach(
                mapable -> {
                    StringJoiner propertyJoiner = new StringJoiner(
                            "",
                            "<" + mapable.getClass().getSimpleName() + ">",
                            "</" + mapable.getClass().getSimpleName() + ">"
                    );
                    mapable.mappedProperties().forEach(
                            (key, value) -> propertyJoiner.add( "<" + key + ">" + value + "</" + key + ">")
                    );
                    objectJoiner.add(propertyJoiner.toString());
                }
        );
        return objectJoiner.toString();
    }

    @Override
    public String formatFileExtension() {
        return "xml";
    }
}
