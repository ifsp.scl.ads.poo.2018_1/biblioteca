package poo.library.report;

import poo.library.report.exporter.ReportExporter;
import poo.library.report.formatter.*;

public class ReportInteractor {
    private ReportInteractor() {

    }

    public static void exportReportToCSVFile() {
        ReportExporter.exportReports(new CSVFormatter());
    }

    public static void exportReportToJSONFile() {
        ReportExporter.exportReports(new JSONFormatter());
    }

    public static void exportReportToXMLFile() {
        ReportExporter.exportReports(new XMLFormatter());
    }
}
