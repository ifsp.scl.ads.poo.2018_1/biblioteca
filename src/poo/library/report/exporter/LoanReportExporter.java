package poo.library.report.exporter;

import poo.library.domain.loan.LoanInteractor;
import poo.library.report.formatter.Formatter;

class LoanReportExporter extends ReportExporter {
    @Override
    protected void exportReport(Formatter formatter) {
        exportToFileWithFormatter(LoanInteractor.getAllLoans(), formatter, "LoanReport");
    }
}
