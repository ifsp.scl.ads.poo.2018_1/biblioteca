package poo.library.report.exporter;

import poo.library.domain.book.BookInteractor;
import poo.library.report.formatter.Formatter;

class BookReportExporter extends ReportExporter {
    @Override
    protected void exportReport(Formatter formatter) {
        exportToFileWithFormatter(BookInteractor.getAllBooks(), formatter, "BookReport");
    }
}
