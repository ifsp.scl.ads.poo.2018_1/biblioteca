package poo.library.report.exporter;

import poo.library.common.model.Mapable;
import poo.library.common.util.Files;
import poo.library.report.formatter.Formatter;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public abstract class ReportExporter {
    private static List<ReportExporter> exporters;

    public static synchronized void exportReports(Formatter formatter) {
        if (exporters == null) {
            exporters = Arrays.asList(
                    new BookReportExporter(),
                    new LoanReportExporter(),
                    new PenaltyReportExporter(),
                    new ReserveReportExporter(),
                    new UserReportExporter()
            );
        }

        for (ReportExporter exporter : exporters) {
            exporter.exportReport(formatter);
        }
    }

    protected abstract void exportReport(Formatter formatter);

    static void exportToFileWithFormatter(List<? extends Mapable> mapables, Formatter formatter, String filenamePrefix) {
        String filename =
                filenamePrefix +
                        "_" + new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date()) +
                        "." + formatter.formatFileExtension();
        Files.saveStringToFile(formatter.formatMapables(mapables), filename);
    }
}
