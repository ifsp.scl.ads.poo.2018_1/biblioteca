package poo.library.report.exporter;

import poo.library.domain.reserve.ReserveInteractor;
import poo.library.report.formatter.Formatter;

class ReserveReportExporter extends ReportExporter {
    @Override
    protected void exportReport(Formatter formatter) {
        exportToFileWithFormatter(ReserveInteractor.getAllReserves(), formatter, "ReserveReport");
    }
}
