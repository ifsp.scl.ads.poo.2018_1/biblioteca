package poo.library.report.exporter;

import poo.library.domain.user.UserInteractor;
import poo.library.report.formatter.Formatter;

class UserReportExporter extends ReportExporter {
    @Override
    protected void exportReport(Formatter formatter) {
        exportToFileWithFormatter(UserInteractor.getAllUsers(), formatter, "UserReport");
    }
}
