package poo.library.report.menu.export;

import poo.library.common.view.OptionViewModel;
import poo.library.report.ReportInteractor;

import java.util.Scanner;

public class ExportReportsAsJSONOption implements OptionViewModel {
    @Override
    public String getTitle() {
        return "JSON";
    }

    @Override
    public void execute(Scanner scanner) {
        ReportInteractor.exportReportToJSONFile();
        System.out.println("Arquivos exportados com sucesso");
    }
}
