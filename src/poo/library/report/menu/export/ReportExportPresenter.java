package poo.library.report.menu.export;

import poo.library.common.view.OptionViewModel;
import poo.library.common.view.Presenter;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ReportExportPresenter extends Presenter {
    private static List<OptionViewModel> options;
    static {
        options = Arrays.asList(
                new ExportReportsAsCSVOption(),
                new ExportReportsAsJSONOption(),
                new ExportReportsAsXMLOption()
        );
    }

    @Override
    protected List<OptionViewModel> options() {
        return options;
    }

    @Override
    public void present(Scanner scanner) {
        System.out.println("Exportar relatórios como:");
        super.present(scanner);
    }
}
