package poo.library.main;

import poo.library.main.presenter.login.LoginContextMainPresenter;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        new LoginContextMainPresenter().present(scanner);
        System.out.println("Até logo!");
        scanner.close();
    }
}
