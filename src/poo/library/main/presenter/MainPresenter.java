package poo.library.main.presenter;

import poo.library.common.view.Presenter;

import java.util.Scanner;

public abstract class MainPresenter extends Presenter {
    @Override
    public void present(Scanner scanner) {
        System.out.println("Lib Sys");
        super.present(scanner);
    }
}
