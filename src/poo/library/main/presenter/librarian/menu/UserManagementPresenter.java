package poo.library.main.presenter.librarian.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.common.view.Presenter;
import poo.library.domain.user.view.menu.AlterarUsuario;
import poo.library.domain.user.view.menu.CadastroUsuario;
import poo.library.domain.user.view.menu.ConsultaUsuario;
import poo.library.domain.user.view.menu.RemoveUsuario;

import java.util.Arrays;
import java.util.List;

public class UserManagementPresenter extends Presenter {
    private static List<OptionViewModel> options;
    static {
        options = Arrays.asList(
                new CadastroUsuario(),
                new AlterarUsuario(),
                new RemoveUsuario(),
                new ConsultaUsuario()
        );
    }

    @Override
    protected List<OptionViewModel> options() {
        return options;
    }
}
