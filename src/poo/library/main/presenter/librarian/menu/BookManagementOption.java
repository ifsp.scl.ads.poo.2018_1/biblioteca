package poo.library.main.presenter.librarian.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.main.presenter.librarian.LibrarianContextMainPresenter;

import java.util.Scanner;

public class BookManagementOption implements OptionViewModel {
    @Override
    public String getTitle() {
        return "Gerenciar livros";
    }

    @Override
    public void execute(Scanner scanner) {
        new BookManagementPresenter().present(scanner);
    }
}
