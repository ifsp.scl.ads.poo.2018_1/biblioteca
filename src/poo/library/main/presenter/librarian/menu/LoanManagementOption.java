package poo.library.main.presenter.librarian.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.main.presenter.librarian.LibrarianContextMainPresenter;

import java.util.Scanner;

public class LoanManagementOption implements OptionViewModel {
    @Override
    public String getTitle() {
        return "Gerenciar empréstimos";
    }

    @Override
    public void execute(Scanner scanner) {
        new LoanManagementPresenter().present(scanner);
    }
}
