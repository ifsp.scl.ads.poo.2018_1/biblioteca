package poo.library.main.presenter.librarian.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.main.presenter.librarian.LibrarianContextMainPresenter;

import java.util.Scanner;

public class UserManagementOption implements OptionViewModel {
    @Override
    public String getTitle() {
        return "Gerenciar usuários";
    }

    @Override
    public void execute(Scanner scanner) {
        new UserManagementPresenter().present(scanner);
    }
}
