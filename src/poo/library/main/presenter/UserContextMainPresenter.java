package poo.library.main.presenter;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.book.view.menu.BookSearchOption;
import poo.library.domain.reserve.menu.ReserveBookOption;

import java.util.Arrays;
import java.util.List;

public class UserContextMainPresenter extends MainPresenter {
    private static List<OptionViewModel> options;
    static {
        options = Arrays.asList(
                new BookSearchOption(),
                new ReserveBookOption()
        );
    }

    @Override
    protected List<OptionViewModel> options() {
        return options;
    }
}
