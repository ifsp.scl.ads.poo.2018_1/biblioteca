package poo.library.main.presenter.login;

import poo.library.common.view.OptionViewModel;
import poo.library.main.presenter.MainPresenter;
import poo.library.main.presenter.login.menu.LoginAsLibrarianOption;
import poo.library.main.presenter.login.menu.LoginAsUserOption;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LoginContextMainPresenter extends MainPresenter {
    private static List<OptionViewModel> options;
    static {
        options = Arrays.asList(
                new LoginAsUserOption(),
                new LoginAsLibrarianOption()
        );
    }

    @Override
    protected List<OptionViewModel> options() {
        return options;
    }
}
