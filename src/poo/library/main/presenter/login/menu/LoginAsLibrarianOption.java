package poo.library.main.presenter.login.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.main.presenter.librarian.LibrarianContextMainPresenter;

import java.util.Scanner;

public class LoginAsLibrarianOption implements OptionViewModel {
    @Override
    public String getTitle() {
        return "Entrar como bibliotecário";
    }

    @Override
    public void execute(Scanner scanner) {
        new LibrarianContextMainPresenter().present(scanner);
    }
}
