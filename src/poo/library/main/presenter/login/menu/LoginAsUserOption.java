package poo.library.main.presenter.login.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.main.presenter.UserContextMainPresenter;

import java.util.Scanner;

public class LoginAsUserOption implements OptionViewModel {
    @Override
    public String getTitle() {
        return "Entrar como usuário";
    }

    @Override
    public void execute(Scanner scanner) {
        new UserContextMainPresenter().present(scanner);
    }
}
