package poo.library.domain.penalty.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.loan.LoanInteractor;
import poo.library.domain.penalty.PenaltyInteractor;

import java.util.Scanner;

public class CadastroMulta implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Registrar multa";
	}

	@Override
	public void execute(Scanner entrada) {
		int codigo;
		int loanCode;
		String descricao;
		
		
		System.out.println("**************************************************");
		System.out.println("Cadastro de multa:");
		System.out.println("**************************************************");

		System.out.println("Informe o codigo da multa: ");
		codigo = entrada.nextInt();

		System.out.println("Informe o codigo do empréstimo: ");
		loanCode = entrada.nextInt();

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

		System.out.println("Informe a descrição da multa: ");
		descricao = entrada.nextLine();

		if (LoanInteractor.getLoanById(loanCode) == null) {
			System.out.println("**********Erro: Empréstimo inexistente**************");

		} else if (PenaltyInteractor.getPenaltyById(codigo) != null) {
			System.out.println("**********Erro: Código já cadastrado**************");

		} else if (PenaltyInteractor.registerPenalty(codigo, loanCode, descricao)) {
			System.out.println("**********Cadastrado com Sucesso!**************");

		} else {
			System.out.println("**********Este empréstimo ainda não está com a devolução em atraso**************");
		}
	}
}
