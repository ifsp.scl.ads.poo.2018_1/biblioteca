package poo.library.domain.penalty.model;

import poo.library.common.model.Mapable;

import java.util.HashMap;
import java.util.Map;

public class Penalty implements Mapable {
    private int id;
    private int userId;
    private String description;
    private double value;

    public Penalty(int id, int userId, String description, double value) {
        this.id = id;
        this.userId = userId;
        this.description = description;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public Map<String, Object> mappedProperties() {
        Map<String, Object> properties = new HashMap<>();

        properties.put("id", id);
        properties.put("id_usuario", userId);
        properties.put("descricao", description);
        properties.put("valor", value);

        // TODO: Put user properties found by id ???

        return properties;
    }
}
