package poo.library.domain.penalty.model;

import poo.library.common.util.ConnectionFactory;
import poo.library.common.model.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PenaltyDAO extends DAO<Penalty> {
    private static PenaltyDAO instance;

    private PenaltyDAO() {
    }

    public static synchronized PenaltyDAO getInstance() {
        if (instance == null) {
            instance = new PenaltyDAO();
        }
        return instance;
    }

    @Override
    public void save(Penalty penalty) {
        String sql = "INSERT INTO multa (id, id_usuario, descricao, valor) VALUES (?, ?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE id_usuario = ?, descricao = ?, valor = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, penalty.getId());
            stmt.setInt(2, penalty.getUserId());
            stmt.setString(3, penalty.getDescription());
            stmt.setDouble(4, penalty.getValue());

            stmt.setInt(5,penalty.getUserId());
            stmt.setString(6, penalty.getDescription());
            stmt.setDouble(7, penalty.getValue());

            stmt.execute();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }

    @Override
    public List<Penalty> getAll() {
        String sql = "SELECT id, id_usuario, descricao, valor FROM multa";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        List<Penalty> penalties = new ArrayList<>();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                Penalty penalty = new Penalty(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getString(3),
                        rs.getDouble(4)
                );
                penalties.add(penalty);
            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }

        return penalties;
    }

    @Override
    public void remove(Penalty penalty) {
        String sql = "delete from multa where id = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, penalty.getId());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }
}
