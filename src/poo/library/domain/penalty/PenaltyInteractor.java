package poo.library.domain.penalty;

import poo.library.common.model.DAOCacheProxy;
import poo.library.common.util.Lists;
import poo.library.domain.book.BookInteractor;
import poo.library.domain.book.model.Livro;
import poo.library.domain.loan.LoanInteractor;
import poo.library.domain.loan.model.Emprestimo;
import poo.library.domain.penalty.model.Penalty;
import poo.library.domain.penalty.model.PenaltyDAO;
import poo.library.domain.user.UserInteractor;
import poo.library.domain.user.model.Usuario;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PenaltyInteractor {
    private PenaltyInteractor() {

    }

    public static boolean registerPenalty(int id, int loanId, String description) {
        Emprestimo loan = LoanInteractor.getLoanById(loanId);
        if (loan == null) {
            return false;
        }

        double value = evaluatePenaltyValue(loan);
        if (value == 0) {
            return false;
        }

        DAOCacheProxy.getInstance(PenaltyDAO.getInstance()).save(new Penalty(id, loan.getId_usuario(), description, value));
        return true;
    }

    public static List<Penalty> getAllPenalties() {
        return DAOCacheProxy.getInstance(PenaltyDAO.getInstance()).getAll();
    }

    public static Penalty getPenaltyById(int id) {
        List<Penalty> penaltyList = Lists.filter(
                DAOCacheProxy.getInstance(
                        PenaltyDAO.getInstance()
                ).getAll(), penalty -> penalty.getId() == id
        );

        if (penaltyList.size() > 0) {
            return penaltyList.get(0);
        }
        return null;
    }

    private static double evaluatePenaltyValue(Emprestimo emprestimo) {
        Date now = new Date();

        if (now.before(emprestimo.getData_devolucao())) {
            return 0;
        }

        long millisDiff = now.getTime() - emprestimo.getData_devolucao().getTime();
        long elapsedDays = TimeUnit.DAYS.convert(millisDiff, TimeUnit.MILLISECONDS);

        double penaltyValue = 0;

        Livro livro = BookInteractor.getBookById(emprestimo.getId_livro());
        switch (livro != null ? livro.getPrioridade() : 0) {
            case 1: // Low
                penaltyValue += 1.0 * elapsedDays;
                break;

            case 2: // Medium
                penaltyValue += 2.0 * elapsedDays;
                break;

            case 3: // High
                penaltyValue += 3.0 * elapsedDays;
                break;

            default:
                break;
        }

        Usuario usuario = UserInteractor.getUserById(emprestimo.getId_usuario());
        switch (usuario != null ? usuario.getCategoria().toLowerCase() : "") {
            case "professor":
                penaltyValue += 0.5 * elapsedDays;
                break;

            case "funcionario":
                penaltyValue += 0.3 * elapsedDays;
                break;

            case "aluno":
                penaltyValue += 0.2 * elapsedDays;
                break;

            case "comunidade":
                penaltyValue += 0.1 * elapsedDays;
                break;

            default:
                break;
        }

        return penaltyValue;
    }
}
