/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.library.domain.book.model;

import poo.library.common.model.Mapable;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ghonorio
 */
public class Livro implements Mapable {
    
    private int id;
    private String titulo;
    private String autor;
    private String categoria;
    private int ano;
    private int prioridade;
    private int disponibilidade;

    public Livro(int id, String titulo, String autor, String categoria, int ano, int prioridade, int disponibilidade) {
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.categoria = categoria;
        this.ano = ano;
        this.prioridade = prioridade;
        this.disponibilidade = disponibilidade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(int prioridade) {
        this.prioridade = prioridade;
    }

    public int getDisponibilidade() {
        return disponibilidade;
    }

    public void setDisponibilidade(int disponibilidade) {
        this.disponibilidade = disponibilidade;
    }

    @Override
    public Map<String, Object> mappedProperties() {
        Map<String, Object> properties = new HashMap<>();

        properties.put("id", id);
        properties.put("titulo", titulo);
        properties.put("autor", autor);
        properties.put("categoria", categoria);
        properties.put("ano", ano);
        properties.put("prioridade", prioridade);
        properties.put("disponibilidade", disponibilidade);

        return properties;
    }
}
