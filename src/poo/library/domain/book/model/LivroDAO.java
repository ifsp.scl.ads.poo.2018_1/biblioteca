package poo.library.domain.book.model;

import poo.library.common.util.ConnectionFactory;
import poo.library.common.model.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LivroDAO extends DAO<Livro> {
    private static LivroDAO instance;

    private LivroDAO() {
    }

    public static synchronized LivroDAO getInstance() {
        if (instance == null) {
            instance = new LivroDAO();
        }
        return instance;
    }

    @Override
    public void save(Livro livro) {
        String sql = "INSERT INTO LIVRO (ID, TITULO, AUTOR, categoria, ANO, prioridade, DISPONIBILIDADE) VALUES (?, ?, ?, ?, ?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE titulo = ? ,autor = ?, categoria = ?, ano = ?, prioridade = ?, disponibilidade = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, livro.getId());
            stmt.setString(2, livro.getTitulo());
            stmt.setString(3, livro.getAutor());
            stmt.setString(4, livro.getCategoria());
            stmt.setInt(5,livro.getAno());
            stmt.setInt(6, livro.getPrioridade());
            stmt.setInt(7, livro.getDisponibilidade());

            stmt.setString(8, livro.getTitulo());
            stmt.setString(9, livro.getAutor());
            stmt.setString(10, livro.getCategoria());
            stmt.setInt(11,livro.getAno());
            stmt.setInt(12, livro.getPrioridade());
            stmt.setInt(13, livro.getDisponibilidade());

            stmt.execute();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }

    @Override
    public List<Livro> getAll() {
        String sql = "SELECT id, titulo, autor, categoria, ano, prioridade, disponibilidade FROM livro";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        List<Livro> livros = new ArrayList<>();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                Livro livro = new Livro(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getInt(6),
                        rs.getInt(7)
                );
                livros.add(livro);
            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }

        return livros;
    }

    @Override
    public void remove(Livro livro) {
        String sql = "delete from livro where id = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, livro.getId());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }
}
