package poo.library.domain.book.view;

import poo.library.domain.book.model.Livro;

import java.util.List;

public final class BookListingUtil {
    private BookListingUtil() {

    }

	public static void list(List<Livro> livros) {
		for (Livro livro : livros) {
            System.out.println("--------------------------");
			System.out.println("ID: " + livro.getId());
			System.out.println("Titulo: " + livro.getTitulo());
			System.out.println("Autor: " + livro.getAutor());
            System.out.println("Categoria: " + livro.getCategoria());
            System.out.println("Ano: " + livro.getAno());
            System.out.println("Prioridade: " + livro.getPrioridade());
			System.out.println("Disponibilidade: " + livro.getDisponibilidade());
			System.out.println("--------------------------");
		}
	}
}