package poo.library.domain.book.view.menu.search;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.book.BookInteractor;
import poo.library.domain.book.view.BookListingUtil;

import java.util.Scanner;

public class ConsultaLivroTitulo implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Título";
	}

	@Override
	public void execute(Scanner entrada) {
		String nome;

		System.out.println("********************************************************");
		System.out.println("Buscar livro por título:");
		System.out.println("********************************************************");

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

        System.out.println("Informe o título do livro: ");
		nome = entrada.nextLine();

        BookListingUtil.list(BookInteractor.getBooksFilteredByTitle(nome));
	}

}
