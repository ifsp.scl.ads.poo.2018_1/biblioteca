package poo.library.domain.book.view.menu.search;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.book.BookInteractor;
import poo.library.domain.book.view.BookListingUtil;

import java.util.Scanner;

public class ConsultaLivroAutor implements OptionViewModel {
	@Override
	public String getTitle() {
		return "Autor";
	}

	@Override
	public void execute(Scanner scanner) {
		String autor;

		System.out.println("********************************************************");
		System.out.println("Buscar livro por autor:");
		System.out.println("********************************************************");

		String cache = scanner.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

        System.out.println("Informe o nome do autor desejado:");
		autor = scanner.nextLine();

        BookListingUtil.list(BookInteractor.getBooksFilteredByAutor(autor));
	}
}
