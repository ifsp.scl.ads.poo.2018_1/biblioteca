package poo.library.domain.book.view.menu.search;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.book.BookInteractor;
import poo.library.domain.book.view.BookListingUtil;

import java.util.Scanner;


public class ConsultaLivroDisponibilidade implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Disponibilidade";
	}

	@Override
	public void execute(Scanner entrada) {
		int disponibilidade = 0;

		System.out.println("*******************************************************************");
		System.out.println("Buscar livro por disponibilidade:");
		System.out.println("*******************************************************************");

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

        boolean validInput = false;
        do {
            System.out.println("Escolha a disponibilidade do livro desejado (livre/emprestado):");
            String aux = entrada.nextLine();

            switch (aux) {
                case "livre":
                    System.out.println("Os livros disponiveis são:");
                    disponibilidade = 1; // FIXME: Define correct value
                    validInput = true;
                    break;

                case "emprestado":
                    System.out.println("Os livros emprestados são:");
                    disponibilidade = 2; // FIXME: Define correct value
                    validInput = true;
                    break;

                default:
                    System.out.println("Opção invalida!");
                    break;
            }
        } while (!validInput);

        BookListingUtil.list(BookInteractor.getBooksFilteredByAvailability(disponibilidade));
	}
}
