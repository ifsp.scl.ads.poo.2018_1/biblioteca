package poo.library.domain.book.view.menu.search;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.book.BookInteractor;
import poo.library.domain.book.view.BookListingUtil;

import java.util.Scanner;


public class ConsultaLivroCategoria implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Categoria";
	}

	@Override
	public void execute(Scanner entrada) {
		String categoria;

		System.out.println("*************************************************************");
		System.out.println("Buscar livro por categoria:");
		System.out.println("*************************************************************");

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

        System.out.println("Informe a categoria do livro desejado: ");
		categoria = entrada.nextLine();

        BookListingUtil.list(BookInteractor.getBooksFilteredByCategory(categoria));
	}

}
