package poo.library.domain.book.view.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.book.BookInteractor;

import java.util.Scanner;

public class AlteraLivro implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Editar livro";
	}

	@Override
	public void execute(Scanner entrada) {
		int codigo;
		String autor;
		String categoria;
		String nome;
		int ano;
		int prioridade;
		int disponibilidade = 0;
		
		System.out.println("**************************************************");
		System.out.println("Alteração de livro:");
		System.out.println("**************************************************");

		System.out.println("Informe o codigo do livro: ");
		codigo = entrada.nextInt();

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

        System.out.println("Informe o título do livro: ");
        nome = entrada.nextLine();

		System.out.println("Informe o autor do Livro: ");
		autor = entrada.nextLine();

		System.out.println("Informe a categoria do Livro: ");
		categoria = entrada.nextLine();

		System.out.println("Informe o ano da edição do Livro: ");
		ano = entrada.nextInt();

		System.out.println("Informe o nivel de prioridade do Livro: ");
		prioridade = entrada.nextInt();

        boolean validInput = false;
        do {
            System.out.println("Informe a disponibilidade do livro (livre/emprestado):");
            String aux = entrada.nextLine();

            switch (aux) {
                case "livre":
                    System.out.println("Os livros disponiveis são:");
                    disponibilidade = 1; // FIXME: Define correct value
                    validInput = true;
                    break;

                case "emprestado":
                    System.out.println("Os livros emprestados são:");
                    disponibilidade = 2; // FIXME: Define correct value
                    validInput = true;
                    break;

                default:
                    System.out.println("Opção invalida!");
                    break;
            }
        } while (!validInput);

        if (BookInteractor.getBookById(codigo) != null) {
            BookInteractor.saveBook(codigo, nome, autor, categoria, ano, prioridade, disponibilidade);
            System.out.println("**********Alterado com Sucesso!**************");

        } else {
            System.out.println("**********Erro: Livro não encontrado**************");
        }
	}
}
