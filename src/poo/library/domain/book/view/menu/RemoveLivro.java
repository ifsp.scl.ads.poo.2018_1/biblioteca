package poo.library.domain.book.view.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.book.BookInteractor;

import java.util.Scanner;

public class RemoveLivro implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Remover livro";
	}

	@Override
	public void execute(Scanner entrada) {
		int codigo;
		
		System.out.println("**************************************************");
		System.out.println("Exclusão de livro:");
		System.out.println("**************************************************");

		System.out.println("Informe o codigo do livro a ser removido: ");
		codigo = entrada.nextInt();

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

        if (BookInteractor.getBookById(codigo) != null) {
            BookInteractor.removeBook(codigo);
            System.out.println("**********Removido com Sucesso!**************");

        } else {
            System.out.println("**********Erro: Livro não encontrado**************");
        }
	}
}
