package poo.library.domain.book.view.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.book.view.menu.search.BookSearchPresenter;

import java.util.Scanner;

public class BookSearchOption implements OptionViewModel {
	@Override
	public String getTitle() {
		return "Consultar livros";
	}

	@Override
	public void execute(Scanner scanner) {
        new BookSearchPresenter().present(scanner);
	}
}
