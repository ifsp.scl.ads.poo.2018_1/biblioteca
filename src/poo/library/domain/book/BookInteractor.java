package poo.library.domain.book;

import poo.library.common.model.DAOCacheProxy;
import poo.library.common.util.Lists;
import poo.library.domain.book.model.Livro;
import poo.library.domain.book.model.LivroDAO;

import java.util.*;

public class BookInteractor {
    private BookInteractor() {

    }

    public static void saveBook(int id, String titulo, String autor, String categoria, int ano, int prioridade, int disponibilidade) {
        Livro livro = new Livro(id, titulo, autor, categoria, ano, prioridade, disponibilidade);
        DAOCacheProxy.getInstance(LivroDAO.getInstance()).save(livro);
    }

    public static void removeBook(int id) {
        Livro livro = new Livro(id, null, null, null, 0, 0, 0);
        DAOCacheProxy.getInstance(LivroDAO.getInstance()).remove(livro);
    }

    public static List<Livro> getAllBooks() {
        return DAOCacheProxy.getInstance(LivroDAO.getInstance()).getAll();
    }

    public static Livro getBookById(int id) {
        List<Livro> livroList = Lists.filter(
                DAOCacheProxy.getInstance(
                        LivroDAO.getInstance()
                ).getAll(), livro -> livro.getId() == id
        );

        if (livroList.size() > 0) {
            return livroList.get(0);
        }
        return null;
    }

    public static List<Livro> getBooksFilteredByTitle(String title) {
        return Lists.filter(
                DAOCacheProxy.getInstance(
                        LivroDAO.getInstance()
                ).getAll(), livro -> livro.getTitulo().toLowerCase().contains(title.toLowerCase())
        );
    }

    public static List<Livro> getBooksFilteredByAutor(String autor) {
        return Lists.filter(
                DAOCacheProxy.getInstance(
                        LivroDAO.getInstance()
                ).getAll(), livro -> livro.getAutor().toLowerCase().contains(autor.toLowerCase())
        );
    }

    public static List<Livro> getBooksFilteredByCategory(String category) {
        return Lists.filter(
                DAOCacheProxy.getInstance(
                        LivroDAO.getInstance()
                ).getAll(), livro -> livro.getCategoria().toLowerCase().contains(category.toLowerCase())
        );
    }

    public static List<Livro> getBooksFilteredByAvailability(int availability) {
        return Lists.filter(
                DAOCacheProxy.getInstance(
                        LivroDAO.getInstance()
                ).getAll(), livro -> livro.getDisponibilidade() == availability
        );
    }
}
