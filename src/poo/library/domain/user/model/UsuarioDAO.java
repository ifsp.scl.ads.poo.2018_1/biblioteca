package poo.library.domain.user.model;

import poo.library.common.util.ConnectionFactory;
import poo.library.common.model.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO extends DAO<Usuario> {
    private static UsuarioDAO instance;

    private UsuarioDAO() {
    }

    public static synchronized UsuarioDAO getInstance() {
        if (instance == null) {
            instance = new UsuarioDAO();
        }
        return instance;
    }

    @Override
    public void save(Usuario user) {
        String sql = "INSERT INTO USUARIO(id, nome, categoria, endereco, telefone, sexo) VALUES (?, ?, ?, ?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE nome = ?, categoria = ?, endereco = ?, telefone = ?, sexo = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, user.getId());
            stmt.setString(2, user.getNome());
            stmt.setString(3, user.getCategoria());
            stmt.setString(4, user.getEndereco());
            stmt.setString(5, user.getTelefone());
            stmt.setObject(6, user.getSexo());

            stmt.setString(7, user.getNome());
            stmt.setString(8, user.getCategoria());
            stmt.setString(9, user.getEndereco());
            stmt.setString(10, user.getTelefone());
            stmt.setObject(11, user.getSexo());

            stmt.execute();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }

    @Override
    public List<Usuario> getAll() {
        String sql = "SELECT id, nome, categoria, endereco, telefone, sexo FROM usuario";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        List<Usuario> usuarios = new ArrayList<>();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                Usuario usuario = new Usuario(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6).charAt(0)
                );
                usuarios.add(usuario);
            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }

        return usuarios;
    }

    @Override
    public void remove(Usuario usuario) {
        String sql = "DELETE FROM usuario WHERE id = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, usuario.getId());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }
}
