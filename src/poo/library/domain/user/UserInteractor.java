package poo.library.domain.user;

import poo.library.common.model.DAOCacheProxy;
import poo.library.common.util.Lists;
import poo.library.domain.user.model.Usuario;
import poo.library.domain.user.model.UsuarioDAO;

import java.util.List;

public class UserInteractor {
    private UserInteractor() {

    }

    public static void saveUser(int id, String nome, String categoria, String endereco, String telefone, char sexo) {
        Usuario usuario = new Usuario(id, nome, categoria, endereco, telefone, sexo);
        DAOCacheProxy.getInstance(UsuarioDAO.getInstance()).save(usuario);
    }

    public static void removeUser(int id) {
        Usuario usuario = new Usuario(id, null, null, null, null, ' ');
        DAOCacheProxy.getInstance(UsuarioDAO.getInstance()).remove(usuario);
    }

    public static List<Usuario> getAllUsers() {
        return DAOCacheProxy.getInstance(UsuarioDAO.getInstance()).getAll();
    }

    public static Usuario getUserById(int id) {
        List<Usuario> usuarioList = Lists.filter(
                DAOCacheProxy.getInstance(
                        UsuarioDAO.getInstance()
                ).getAll(), usuario -> usuario.getId() == id
        );

        if (usuarioList.size() > 0) {
            return usuarioList.get(0);
        }
        return null;
    }

    public static List<Usuario> getUsersFilteredByName(String name) {
        return Lists.filter(
                DAOCacheProxy.getInstance(
                        UsuarioDAO.getInstance()
                ).getAll(), usuario -> usuario.getNome().toLowerCase().contains(name.toLowerCase())
        );
    }
}
