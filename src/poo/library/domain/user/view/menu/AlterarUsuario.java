package poo.library.domain.user.view.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.user.UserInteractor;

import java.util.Scanner;

public class AlterarUsuario implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Editar usuário";
	}

	@Override
	public void execute(Scanner entrada) {
		int codigo;
        char sexo;
        String nome;
		String categoria;
		String endereco;
		String telefone;
		
		System.out.println("**************************************************");
		System.out.println("Alteração de usuário:");
		System.out.println("**************************************************");

        String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

        System.out.println("Informe o codigo do usuário: ");
        codigo = entrada.nextInt();

		System.out.println("Informe o nome do usuário: ");
		nome = entrada.nextLine();

		System.out.println("Informe o genero do usuário: ");
		sexo = entrada.nextLine().charAt(0);

		System.out.println("Informe a categoria do usuário: ");
		categoria = entrada.nextLine();

		System.out.println("Informe o endereço do usuário: ");
		endereco = entrada.nextLine();

		System.out.println("Informe o telefone do usuário: ");
		telefone = entrada.nextLine();

		if (UserInteractor.getUserById(codigo) != null) {
			UserInteractor.saveUser(codigo, nome, categoria, endereco, telefone, sexo);
			System.out.println("**********Alterado com Sucesso!**************");

		} else {
			System.out.println("**********Erro: Usuário não encontrado**************");
		}
	}
}
