package poo.library.domain.user.view.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.user.UserInteractor;

import java.util.Scanner;

public class RemoveUsuario implements OptionViewModel {

    @Override
    public String getTitle() {
        return "Remover usuário";
    }

    @Override
	public void execute(Scanner entrada) {
		int codigo;
		
		System.out.println("**************************************************");
		System.out.println("Remoção de usuário:");
		System.out.println("**************************************************");

		System.out.println("Informe o codigo do usuário a ser removido:");
		codigo = entrada.nextInt();

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

		if (UserInteractor.getUserById(codigo) != null) {
			UserInteractor.removeUser(codigo);
			System.out.println("**********Removido com Sucesso!**************");

		} else {
			System.out.println("**********Erro: Livro não encontrado**************");
		}
	}
}
