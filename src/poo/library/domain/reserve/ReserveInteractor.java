package poo.library.domain.reserve;

import poo.library.common.model.DAOCacheProxy;
import poo.library.common.util.Lists;
import poo.library.domain.reserve.model.Reserve;
import poo.library.domain.reserve.model.ReserveDAO;

import java.util.Date;
import java.util.List;

public class ReserveInteractor {
    private ReserveInteractor() {

    }

    public static void makeReserve(int id, int bookId, int userId) {
        DAOCacheProxy.getInstance(ReserveDAO.getInstance()).save(new Reserve(id, userId, bookId, new Date()));
    }

    public static List<Reserve> getAllReserves() {
        return DAOCacheProxy.getInstance(ReserveDAO.getInstance()).getAll();
    }

    public static Reserve getReserveById(int id) {
        List<Reserve> reserveList = Lists.filter(
                DAOCacheProxy.getInstance(
                        ReserveDAO.getInstance()
                ).getAll(), reserve -> reserve.getId() == id
        );

        if (reserveList.size() > 0) {
            return reserveList.get(0);
        }
        return null;
    }
}
