package poo.library.domain.reserve.model;

import poo.library.common.util.ConnectionFactory;
import poo.library.common.model.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReserveDAO extends DAO<Reserve> {
    private static ReserveDAO instance;

    private ReserveDAO() {
    }

    public static synchronized ReserveDAO getInstance() {
        if (instance == null) {
            instance = new ReserveDAO();
        }
        return instance;
    }

    @Override
    public void save(Reserve reserve) {
        String sql = "INSERT INTO reserva (id, id_usuario, id_livro, data) VALUES (?, ?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE id_usuario = ? ,id_livro = ?, data = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, reserve.getId());
            stmt.setInt(2, reserve.getUserId());
            stmt.setInt(3, reserve.getBookId());
            stmt.setDate(4, new Date(reserve.getDate().getTime()));

            stmt.setInt(5, reserve.getUserId());
            stmt.setInt(6, reserve.getBookId());
            stmt.setDate(7, new Date(reserve.getDate().getTime()));

            stmt.execute();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }

    @Override
    public List<Reserve> getAll() {
        String sql = "SELECT id, id_usuario, id_livro, data FROM reserva";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        List<Reserve> reserves = new ArrayList<>();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                Reserve reserve = new Reserve(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        new java.util.Date(rs.getDate(4).getTime())
                );
                reserves.add(reserve);
            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }

        return reserves;
    }

    @Override
    public void remove(Reserve reserve) {
        String sql = "delete from reserva where id = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, reserve.getId());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }
}
