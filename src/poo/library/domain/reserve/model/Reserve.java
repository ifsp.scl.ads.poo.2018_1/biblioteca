package poo.library.domain.reserve.model;

import poo.library.common.model.Mapable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Reserve implements Mapable {
    private int id, userId, bookId;
    private Date date;

    public Reserve(int id, int userId, int bookId, Date date) {
        this.id = id;
        this.userId = userId;
        this.bookId = bookId;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public Map<String, Object> mappedProperties() {
        Map<String, Object> properties = new HashMap<>();

        properties.put("id", id);
        properties.put("id_usuario", userId);
        properties.put("id_livro", bookId);
        properties.put("data", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(date));

        // TODO: Put user/book properties found by id ???

        return properties;
    }
}
