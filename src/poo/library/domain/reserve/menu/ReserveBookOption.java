package poo.library.domain.reserve.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.reserve.ReserveInteractor;

import java.util.Scanner;

public class ReserveBookOption implements OptionViewModel {
    @Override
    public String getTitle() {
        return "Reservar livro";
    }

    @Override
    public void execute(Scanner scanner) {
        int codigo;
        int codigolivro;
        int codigousuario;

        System.out.println("**************************************************");
        System.out.println("Reserva de livro:");
        System.out.println("**************************************************");

        System.out.println("Informe o codigo da reserva: ");
        codigo = scanner.nextInt();

        System.out.println("Informe o codigo do livro: ");
        codigolivro = scanner.nextInt();

        System.out.println("Informe o codigo do usuário: ");
        codigousuario = scanner.nextInt();

        if (ReserveInteractor.getReserveById(codigo) == null) {
            ReserveInteractor.makeReserve(codigo, codigolivro, codigousuario);
            System.out.println("**********Reservado com Sucesso!**************");

        } else {
            System.out.println("**********Erro: Reserva já cadastrada**************");
        }
    }
}
