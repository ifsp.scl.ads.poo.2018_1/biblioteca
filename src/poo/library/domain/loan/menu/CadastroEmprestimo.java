package poo.library.domain.loan.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.loan.LoanInteractor;

import java.util.Scanner;

public class CadastroEmprestimo implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Registrar empréstimo";
	}

	@Override
	public void execute(Scanner entrada) {
		int codigo;
		int codigolivro;
		int codigousuario;

		System.out.println("**************************************************");
		System.out.println("Cadastro de empréstimo:");
		System.out.println("**************************************************");

		System.out.println("Informe o codigo do empréstimo: ");
		codigo = entrada.nextInt();

		System.out.println("Informe o codigo do livro: ");
		codigolivro = entrada.nextInt();

		System.out.println("Informe o codigo do usuário: ");
		codigousuario = entrada.nextInt();

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

        if (LoanInteractor.getLoanById(codigo) == null) {
            LoanInteractor.makeLoan(codigo, codigolivro, codigousuario);
            System.out.println("**********Registrado com Sucesso!**************");

        } else {
            System.out.println("**********Erro: Empréstimo já cadastrado**************");
        }
	}
}
