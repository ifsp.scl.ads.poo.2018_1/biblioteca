package poo.library.domain.loan.menu;

import poo.library.common.view.OptionViewModel;
import poo.library.domain.loan.LoanInteractor;
import poo.library.domain.loan.view.LoanListingUtil;

import java.util.Scanner;


public class ConsultaEmprestimo implements OptionViewModel {

	@Override
	public String getTitle() {
		return "Consultar empréstimos";
	}

	@Override
	public void execute(Scanner entrada) {
		int codigo;

		System.out.println("********************************************************");
		System.out.println("Consulta de empréstimos:");
		System.out.println("********************************************************");

		String cache = entrada.nextLine();//consome o buffer deixado pelo Enter para corrigiro o problema de buffer de entrada

		System.out.println("Informe o código do usuário desejado:");
		codigo = entrada.nextInt();

		LoanListingUtil.list(LoanInteractor.getLoanByUserId(codigo));
	}
}
