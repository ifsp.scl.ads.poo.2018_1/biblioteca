package poo.library.domain.loan;

import poo.library.common.model.DAOCacheProxy;
import poo.library.common.util.Lists;
import poo.library.domain.loan.model.Emprestimo;
import poo.library.domain.loan.model.EmprestimoDAO;
import poo.library.domain.user.UserInteractor;
import poo.library.domain.user.model.Usuario;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LoanInteractor {
    private LoanInteractor() {

    }

    public static void makeLoan(int id, int bookId, int userId) {
        Emprestimo emprestimo = new Emprestimo(id, userId, bookId, new Date(), null);
        emprestimo.setData_devolucao(evaluateReturnDate(emprestimo));
        DAOCacheProxy.getInstance(EmprestimoDAO.getInstance()).save(emprestimo);
    }

    public static List<Emprestimo> getAllLoans() {
        return DAOCacheProxy.getInstance(EmprestimoDAO.getInstance()).getAll();
    }

    public static Emprestimo getLoanById(int id) {
        List<Emprestimo> loanList = Lists.filter(
                DAOCacheProxy.getInstance(
                        EmprestimoDAO.getInstance()
                ).getAll(), loan -> loan.getId() == id
        );

        if (loanList.size() > 0) {
            return loanList.get(0);
        }
        return null;
    }

    public static List<Emprestimo> getLoanByUserId(int userId) {
        return Lists.filter(
                DAOCacheProxy.getInstance(
                        EmprestimoDAO.getInstance()
                ).getAll(), loan -> loan.getId_usuario() == userId
        );
    }

	private static Date evaluateReturnDate(Emprestimo emprestimo) {
		Usuario usuario = UserInteractor.getUserById(emprestimo.getId_usuario());

        int daysToAdd = 0;
		switch (usuario != null ? usuario.getCategoria().toLowerCase() : "") {
			case "professor":
			    daysToAdd = 15;
				break;

			case "funcionario":
                daysToAdd = 12;
				break;

            case "aluno":
                daysToAdd = 20;
                break;

            case "comunidade":
                daysToAdd = 8;
                break;

			default:
				break;
		}

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(emprestimo.getData_emprestimo());
        calendar.add(Calendar.DAY_OF_MONTH, daysToAdd);
		return calendar.getTime();
	}
}

