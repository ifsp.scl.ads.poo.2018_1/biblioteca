package poo.library.domain.loan.view;

import poo.library.domain.loan.model.Emprestimo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public final class LoanListingUtil {
    private LoanListingUtil() {

    }

	public static void list(List<Emprestimo> emprestimos) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		for (Emprestimo emprestimo : emprestimos) {
            System.out.println("--------------------------");
			System.out.println("ID: " + emprestimo.getId());
			System.out.println("ID Usuário: " + emprestimo.getId_usuario());
			System.out.println("ID Livro: " + emprestimo.getId_livro());
            System.out.println("Retirada: " + format.format(emprestimo.getData_emprestimo()));
            System.out.println("Devolução: " + format.format(emprestimo.getData_devolucao()));
			System.out.println("--------------------------");
		}
	}
}