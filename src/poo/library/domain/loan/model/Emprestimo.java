package poo.library.domain.loan.model;

import poo.library.common.model.Mapable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Emprestimo implements Mapable {
    private int id;
    private int id_usuario;
    private int id_livro;
    private Date data_emprestimo;
    private Date data_devolucao;

    public Emprestimo(int id, int id_usuario, int id_livro, Date data_emprestimo, Date data_devolucao) {
        this.id = id;
        this.id_usuario = id_usuario;
        this.id_livro = id_livro;
        this.data_emprestimo = data_emprestimo;
        this.data_devolucao = data_devolucao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_livro() {
        return id_livro;
    }

    public void setId_livro(int id_livro) {
        this.id_livro = id_livro;
    }

    public Date getData_emprestimo() {
        return data_emprestimo;
    }

    public void setData_emprestimo(Date data_emprestimo) {
        this.data_emprestimo = data_emprestimo;
    }

    public Date getData_devolucao() {
        return data_devolucao;
    }

    public void setData_devolucao(Date data_devolucao) {
        this.data_devolucao = data_devolucao;
    }

    @Override
    public Map<String, Object> mappedProperties() {
        Map<String, Object> properties = new HashMap<>();

        properties.put("id", id);
        properties.put("id_usuario", id_usuario);
        properties.put("id_livro", id_livro);
        properties.put("data_emprestimo", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(data_emprestimo));
        properties.put("data_devolucao", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(data_devolucao));

        // TODO: Put user/book properties found by id ???

        return properties;
    }
}
