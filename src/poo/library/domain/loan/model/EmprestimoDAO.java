package poo.library.domain.loan.model;

import poo.library.common.util.ConnectionFactory;
import poo.library.common.model.DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmprestimoDAO extends DAO<Emprestimo> {
    private static EmprestimoDAO instance;

    private EmprestimoDAO() {
    }

    public static synchronized EmprestimoDAO getInstance() {
        if (instance == null) {
            instance = new EmprestimoDAO();
        }
        return instance;
    }

    @Override
    public void save(Emprestimo emprestimo) {
        String sql = "INSERT INTO emprestimo (id, id_usuario, id_livro, data_emprestimo, data_devolucao) VALUES (?, ?, ?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE id_usuario = ? ,id_livro = ?, data_emprestimo = ?, data_devolucao = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);


            stmt.setInt(1, emprestimo.getId());
            stmt.setInt(2, emprestimo.getId_usuario());
            stmt.setInt(3, emprestimo.getId_livro());
            stmt.setDate(4, new Date(emprestimo.getData_emprestimo().getTime()));
            stmt.setDate(5, new Date(emprestimo.getData_devolucao().getTime()));

            stmt.setInt(6, emprestimo.getId_usuario());
            stmt.setInt(7, emprestimo.getId_livro());
            stmt.setDate(8, new Date(emprestimo.getData_emprestimo().getTime()));
            stmt.setDate(9, new Date(emprestimo.getData_devolucao().getTime()));

            stmt.execute();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }

    @Override
    public List<Emprestimo> getAll() {
        String sql = "SELECT id, id_usuario, id_livro, data_emprestimo, data_devolucao FROM emprestimo";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        List<Emprestimo> emprestimos = new ArrayList<>();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while(rs.next()) {
                Emprestimo emprestimo = new Emprestimo(
                        rs.getInt(1),
                        rs.getInt(2),
                        rs.getInt(3),
                        new java.util.Date(rs.getDate(4).getTime()),
                        new java.util.Date(rs.getDate(5).getTime())
                );
                emprestimos.add(emprestimo);
            }
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }

        return emprestimos;
    }

    @Override
    public void remove(Emprestimo emprestimo) {
        String sql = "delete from emprestimo where id = ?";
        Connection conn = ConnectionFactory.getInstance().getConnection();

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, emprestimo.getId());
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            ConnectionFactory.getInstance().close(conn);
        }
    }
}






