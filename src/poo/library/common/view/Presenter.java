package poo.library.common.view;

import java.util.List;
import java.util.Scanner;

public abstract class Presenter {
    private List<OptionViewModel> options = options();

    protected abstract List<OptionViewModel> options();

    public void present(Scanner scanner) {
        int selectedOption;
        do {
            System.out.println("Escolha uma opção:");
            System.out.println("0 - Sair");
            for (OptionViewModel option : options) {
                System.out.println(options.indexOf(option) + 1 + " - " + option.getTitle());
            }
            selectedOption = scanner.nextInt();
            System.out.println("___________________________");

            if (selectedOption > 0 && selectedOption - 1 < options.size()) {
                options.get(selectedOption - 1).execute(scanner);
            } else if (selectedOption != 0) {
                System.out.println("Opção inválida");
            }
            System.out.println("___________________________");
        } while (selectedOption != 0);
    }
}
