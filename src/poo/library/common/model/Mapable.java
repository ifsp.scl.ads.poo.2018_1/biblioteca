package poo.library.common.model;

import java.util.Map;

public interface Mapable {
    public Map<String, Object> mappedProperties();
}
