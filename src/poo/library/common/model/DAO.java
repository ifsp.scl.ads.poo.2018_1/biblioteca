package poo.library.common.model;

import java.util.List;

public abstract class DAO<E> {
    public abstract void save(E entity);
    public abstract List<E> getAll();
    public abstract void remove(E entity);
}
