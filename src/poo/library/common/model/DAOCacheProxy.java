package poo.library.common.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAOCacheProxy<E> extends DAO<E> {
    private static Map<Class, DAOCacheProxy> instances = new HashMap<>();
    private List<E> cache;
    private DAO<E> dao;

    private DAOCacheProxy(DAO<E> dao) {
        this.dao = dao;
    }

    public static synchronized <E> DAOCacheProxy<E> getInstance(DAO<E> dao) {
        if (instances.get(dao.getClass()) == null) {
            instances.put(dao.getClass(), new DAOCacheProxy<>(dao));
        }
        return instances.get(dao.getClass());
    }

    private void invalidateCache() {
        cache = null;
    }

    @Override
    public void save(E entity) {
        dao.save(entity);
        invalidateCache();
    }

    @Override
    public List<E> getAll() {
        if (cache == null) {
            cache = dao.getAll();
        }
        return Collections.unmodifiableList(cache);
    }

    @Override
    public void remove(E entity) {
        dao.remove(entity);
        invalidateCache();
    }
}
