package poo.library.common.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	
    private static ConnectionFactory factory;

    private ConnectionFactory() {}

    public static synchronized ConnectionFactory getInstance() {
        if(factory == null)
            factory = new ConnectionFactory();
        return factory;
    }

    public Connection getConnection() {
        Connection conn;
        try {
            conn = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/bibliotecadb","SA","");
            return conn;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void close(Connection conn) {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}








